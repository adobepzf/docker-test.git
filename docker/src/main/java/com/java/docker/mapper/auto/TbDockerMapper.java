package com.java.docker.mapper.auto;

import com.java.docker.model.auto.TbDocker;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author AdobePeng
 * @since 2021-04-24
 */
public interface TbDockerMapper extends BaseMapper<TbDocker> {

}
