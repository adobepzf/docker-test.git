package com.java.docker.service.impl;

import com.java.docker.model.auto.TbDocker;
import com.java.docker.mapper.auto.TbDockerMapper;
import com.java.docker.service.ITbDockerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author AdobePeng
 * @since 2021-04-24
 */
@Service
public class TbDockerServiceImpl extends ServiceImpl<TbDockerMapper, TbDocker> implements ITbDockerService {

}
