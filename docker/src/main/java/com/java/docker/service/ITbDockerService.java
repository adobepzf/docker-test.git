package com.java.docker.service;

import com.java.docker.model.auto.TbDocker;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author AdobePeng
 * @since 2021-04-24
 */
public interface ITbDockerService extends IService<TbDocker> {

}
