package com.java.docker.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.java.docker.model.auto.TbDocker;
import com.java.docker.service.ITbDockerService;
import com.java.docker.service.ITbDockerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author AdobePeng
 * @since 2021-04-24
 */
@RestController
@RequestMapping("/tb-docker")
public class TbDockerController {
    @Autowired
    private ITbDockerService tbDockerService;

    @GetMapping("/testOne")
    public String testOne(){
        List<TbDocker> list = tbDockerService.list();
        return list.get(0).toString();
    }
    @GetMapping("/insertOne")
    public void insertOne(){
        TbDocker dao = new TbDocker();
        dao.setName("李四");
        dao.setAge(24);
        tbDockerService.save(dao);
    }
    @GetMapping("/updateOne")
    public void updateOne(){
        TbDocker byId = tbDockerService.getById(1);
        byId.setAge(100);
        tbDockerService.updateById(byId);
    }

    @GetMapping("/list")
    public List<TbDocker> list(){
        return tbDockerService.list();
    }


    @GetMapping("/page")
    public List<TbDocker> page(){
        Page<TbDocker> page = new Page<>(1,2);
        Page<TbDocker> page1 = tbDockerService.page(page, null);

        return tbDockerService.list();
    }

    @GetMapping("/deleteOne")
    private void deleteOne(){
        tbDockerService.removeById(1);
    }

    @GetMapping("/testTwo")
    public String testTwo(){
        return "Hello World";
    }
}
